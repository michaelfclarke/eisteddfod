package uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen;

import uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen.db.Lesson;
import uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen.db.Phrase;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class LessonActivity extends Activity {

	PhraseListAdapter phraseAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_window);
		
		phraseAdapter = new PhraseListAdapter(getBaseContext(), 
				Phrase.getPhraseByLessonNumber(getBaseContext(), 
										Tracker.getCurrentLessonId()));
		
		final ListView listView = (ListView)findViewById(R.id.generic_list);
		listView.setAdapter(phraseAdapter);
		this.getActionBar().setDisplayHomeAsUpEnabled(true);
		
		this.setTitle("Lesson " + Tracker.getCurrentLessonId() + ": "
				+ Lesson.getLesson(getBaseContext(), 
						Tracker.getCurrentLessonId()).getLessonTitle());
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, 
											View arg1, int arg2, long arg3) {
		
				Phrase p = (Phrase)phraseAdapter.getItem(arg2);
				p.getAudio(getBaseContext()).start(); 
				
			}
			
		});
		
		Button lessonButton = (Button)findViewById(R.id.lessonButton);
		Button quizButton = (Button)findViewById(R.id.quizButton);
		Button aboutButton = (Button)findViewById(R.id.aboutButton);
		lessonButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), LessonActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
		quizButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), QuizActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
		aboutButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), AboutActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
		
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if (item.getItemId() == android.R.id.home) {
	        Intent i = new Intent(getApplicationContext(), LessonPickerActivity.class);
	        startActivity(i);
	        this.overridePendingTransition(0, 0);
	        return true;
		}
		
		return super.onOptionsItemSelected(item);
		
	}
	
	public void onResume() {
		
        super.onResume();
		
		this.overridePendingTransition(0, 0);
		
		phraseAdapter.setPhraseList(
			Phrase.getPhraseByLessonNumber(getBaseContext(), 
									Tracker.getCurrentLessonId()));
		phraseAdapter.notifyDataSetChanged();
		this.setTitle("Lesson " + Tracker.getCurrentLessonId() + ": "
				+ Lesson.getLesson(getBaseContext(), 
						Tracker.getCurrentLessonId()).getLessonTitle());
		
	}
	
}
