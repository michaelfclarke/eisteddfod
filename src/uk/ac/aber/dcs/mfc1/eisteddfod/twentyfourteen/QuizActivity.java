package uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen;

import java.util.ArrayList;
import java.util.Random;

import uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen.db.Lesson;
import uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen.db.Phrase;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class QuizActivity extends Activity {
	
	private TextView englishText;
	private TextView welshText;
	private Button nextButton;
	private ArrayList<Phrase> phrases;
	private int question;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.question_answer);		
		
		englishText = (TextView) findViewById(R.id.answerText);
		welshText = (TextView) findViewById(R.id.englishTextQuestion);
		nextButton = (Button)findViewById(R.id.button1);

		phrases = new ArrayList<Phrase>();
		
		for (int i = 0; i < Tracker.getCurrentLessonId(); i++)
				phrases.addAll(
						Phrase.getPhraseByLessonNumber(
											getBaseContext(), i+1));
		
		//nextQuestion();
		
		nextButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if (englishText.getText().equals(" ")) showAnswer();
				else					   nextQuestion();
				
			}
		});
		
		this.getActionBar().setDisplayHomeAsUpEnabled(true);
	
		Button lessonButton = (Button)findViewById(R.id.lessonButton);
		Button quizButton = (Button)findViewById(R.id.quizButton);
		Button aboutButton = (Button)findViewById(R.id.aboutButton);
		lessonButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), LessonActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
		quizButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), QuizActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
		aboutButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), AboutActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
		
		this.setTitle("Lesson " + Tracker.getCurrentLessonId() + ": "
				+ Lesson.getLesson(getBaseContext(), 
						Tracker.getCurrentLessonId()).getLessonTitle());
		
	}
	

	public void onResume() {
        super.onResume();
        
        
        
		this.overridePendingTransition(0, 0);
		
		/* phrases = new ArrayList<Phrase>();
		for (int i = 0; i < Tracker.getCurrentLessonId(); i++)
			phrases.addAll(
					Phrase.getPhraseByLessonNumber(
										getBaseContext(), i+1)); */
		
		phrases = Phrase.getPhraseByLessonNumber(
					getBaseContext(), Tracker.getCurrentLessonId());
		
		this.setTitle("Lesson " + Tracker.getCurrentLessonId() + ": "
				+ Lesson.getLesson(getBaseContext(), 
						Tracker.getCurrentLessonId()).getLessonTitle());
		
		this.nextQuestion();
		
	}
	
	private void nextQuestion() { 
		
		final Random r = new Random();
		this.question = r.nextInt(phrases.size());
		
		runOnUiThread(new Runnable() {
			public void run() {
				phrases.get(question).getAudio(getBaseContext()).start();
				welshText.setText(phrases.get(question).getWelsh());
				englishText.setText(" ");
				nextButton.setText("Show Answer");
			}
				
		});
		
	}
	
	private void showAnswer() {
		
		runOnUiThread(new Runnable() {
			public void run() {
				englishText.setText(phrases.get(question).getEnglish());
				nextButton.setText("Next Question");
			}
		});
		
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if (item.getItemId() == android.R.id.home) {
	        Intent i = new Intent(getApplicationContext(), LessonPickerActivity.class);
	        startActivity(i);
	        this.overridePendingTransition(0, 0);
	        return true;
		}
		
		return super.onOptionsItemSelected(item);
		
	}

	
}
