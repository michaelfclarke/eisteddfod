package uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen;

import java.util.ArrayList;

import uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen.db.About;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

public class AboutActivity extends Activity {

	AboutListAdapter aboutAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_window);		
		
		final ArrayList<About> aboutList = new ArrayList<About>();
		
		aboutList.add(new About("html/Welcome.html", "Gwybodaeth gyffredinol am yr Eisteddfod", 
											"General information about the Eisteddfod"));
		
		aboutList.add(new About("html/Llanelli.html", "Ymweld â Sir Drefaldwyn",
				"Visiting Montgomeryshire"));
		
		aboutList.add(new About("html/DysguCymraeg.html", "Dysgu Cymraeg - Beth Amdani?",
													"Learn Welsh - How about it?"));
		
		aboutAdapter = new AboutListAdapter(getBaseContext(), aboutList);	
		ListView listView = (ListView)findViewById(R.id.about_list);
		listView.setAdapter(aboutAdapter);
		
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				About a = aboutList.get(arg2);
				if (a == null) {
					Log.e("APP", "Error, a is null!");
				}
				Tracker.setAbout(a);
				
				Intent i = new Intent(getBaseContext(), HTMLActivity.class);
				startActivity(i);
				overridePendingTransition(0, 0);
					
			}
		});
		
		Button lessonButton = (Button)findViewById(R.id.lessonButton);
		Button quizButton = (Button)findViewById(R.id.quizButton);
		Button aboutButton = (Button)findViewById(R.id.aboutButton);
		lessonButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), LessonActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
		quizButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), QuizActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
		aboutButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), AboutActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
				
	}
	

	public void onResume() {
        super.onResume();
		this.overridePendingTransition(0, 0);
	}

}
