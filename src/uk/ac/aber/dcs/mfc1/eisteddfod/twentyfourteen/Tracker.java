package uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen;

import uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen.db.About;

public class Tracker {

	static private int currentLessonId = 1;
	static private About a = null;
	
	
	static public int getCurrentLessonId() {
		return currentLessonId;
	}
	
	static public void setCurrentLessonId(final int currentLessonId) {
		Tracker.currentLessonId = currentLessonId;
	}
	
	static public void setAbout(About a) {
		Tracker.a = a;
	}
	
	static public About getAbout() {
		return Tracker.a;
	}
	
	
	private Tracker() {}
	
}
