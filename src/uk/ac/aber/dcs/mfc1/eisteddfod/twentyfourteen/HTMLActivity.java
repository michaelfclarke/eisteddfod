package uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen;

import java.io.InputStream;

import uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen.db.About;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;

public class HTMLActivity extends Activity {
	
	private WebView webView = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.html_area);	
		this.webView = (WebView) findViewById(R.id.webView1);
		WebSettings settings = webView.getSettings();
		settings.setJavaScriptEnabled(true);
		About a = Tracker.getAbout();
		if (a == null) {
			Log.e("ERROR", "a is null!!");
		}
		if (a.getUrl() == null) {
			Log.e("ERROR", "a.getUrl() returned null!");
		}
		this.setUrl(Tracker.getAbout().getUrl());
		
		this.getActionBar().setDisplayHomeAsUpEnabled(true);
		
		Button lessonButton = (Button)findViewById(R.id.lessonButton);
		Button quizButton = (Button)findViewById(R.id.quizButton);
		Button aboutButton = (Button)findViewById(R.id.aboutButton);
		lessonButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), LessonActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
		quizButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), QuizActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
		aboutButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), AboutActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
		
	}
	
	public void setUrl(String url) {
		
		String fileData = "";
	
		try {
			
			InputStream is = this.getAssets().open(url);
			while (is.available() > 0)
				fileData += (char)is.read();
			is.close();
			
		} catch (Exception e) {
			
			System.err.printf("Error loading data.\nError: %s\n" + 
					"Stack Trace:\n%s", e.toString(), e.getStackTrace());
			
		}

		this.webView.loadDataWithBaseURL("file:///android_asset/html/", fileData, "text/html", "UTF-8", null);
		//this.webView.loadData(fileData, "text/html; charset=utf-8", null);
		
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if (item.getItemId() == android.R.id.home) {
			finish();
	        this.overridePendingTransition(0, 0);
	        return true;
		}
		
		return super.onOptionsItemSelected(item);
		
	}

}
