package uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen;

import uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen.db.Lesson;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class LessonPickerActivity extends Activity {

	LessonListAdapter lessonAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_window);		
		
		lessonAdapter = new LessonListAdapter(getBaseContext(),
								Lesson.getAllLessons(getBaseContext()));
		
		ListView listView = (ListView)findViewById(R.id.generic_list);
		listView.setAdapter(lessonAdapter);
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, 
											View arg1, int arg2, long arg3) {
				
				Lesson l = (Lesson)lessonAdapter.getItem(arg2);
				Tracker.setCurrentLessonId(l.getLessonNumber());
				finish();
				
			}
			
		});
		
		Button lessonButton = (Button)findViewById(R.id.lessonButton);
		Button quizButton = (Button)findViewById(R.id.quizButton);
		Button aboutButton = (Button)findViewById(R.id.aboutButton);
		lessonButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), LessonActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
		quizButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), QuizActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
		aboutButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), AboutActivity.class);
		        startActivity(i);
		        overridePendingTransition(0, 0);
			}
		});
	
		this.setTitle("Select Lesson");
		
	}

	public void onResume() {
		
        super.onResume();
		
		this.overridePendingTransition(0, 0);
		
	}
	
}
