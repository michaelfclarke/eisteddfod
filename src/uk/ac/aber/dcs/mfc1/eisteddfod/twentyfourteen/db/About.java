package uk.ac.aber.dcs.mfc1.eisteddfod.twentyfourteen.db;

public class About {

	private String headingText;
	private String descriptionText;
	private String url;
	
	public About(String url, final String headingText, final String descriptionText) {
		this.url = url;
		this.setHeadingText(headingText);
		this.setDescriptionText(descriptionText);
	}
	
	public String getUrl() {
		return this.url;
	}
	
	private void setHeadingText(final String headingText) {
		this.headingText = headingText;
	}
	
	private void setDescriptionText(final String descriptionText) {
		this.descriptionText = descriptionText;
	}
	
	public final String getHeadingText() {
		return this.headingText;
	}
	
	public final String getDescriptionText() {
		return this.descriptionText;
	}
	
}
